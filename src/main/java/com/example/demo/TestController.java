package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Copyright © 2019新东网. All rights reserved.
 *
 * @author create by hyq
 * @version 1.0
 * @date 2019/6/1
 * @description:
 */
@RestController
public class TestController {
    @RequestMapping("/hello")
    public  String hello(){
        return "hello Jenkins";
    }

}
